#ifndef _CY8TMG120_H_
#define _CY8TMG120_H_

#include <stdint.h>

uint8_t TMG120_ReadFwVer(void);
void TMG120_Init(void);
void Clear_INT_Flag(void);
void TMG120_ReadPointData(void);
void TMG120_DataHandle(void);

#endif
