#ifndef R61581_H
#define R61581_H

#include <stdint.h>

void r61581_test(void);
void r61851_init(void);
void r61581_disp_pic(uint16_t* p_dat, uint32_t len);
void r61581_disp_update(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t* p_buf);

#endif /* R61581_H */