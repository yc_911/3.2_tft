#ifndef _TFT_DRV_H_
#define _TFT_DRV_H_


void tft_init(uint8_t tft_d0_pin, uint8_t tft_wr_pin, uint16_t clock_div);
void tft_set_mode_single_byte();
void tft_set_mode_double_byte();
void tft_write(uint16_t value);
void tft_multi_write(const uint8_t *keys, uint32_t n, uint16_t *map);
void tft_multi_write_nbytes(const uint16_t *values, uint32_t n);
void multi_write_repeat(uint16_t value, uint32_t n);
void tft_flush();

#endif